#!/usr/bin/env python
# -*- coding: utf-8 -*-

import mc2py.svld as mc
import math
import codecs
import yaml

def Calculate(d):
    l = []
    flag = 0
    omega = 2 * 50 * math.pi  # Частота
    W_1 = 1000  # Кол-во витков первычной обмотки
    Sn = d[u"Sn"] * 10 ** 6  # Номинальная мощность
    Uhin = d[u"Uhin"] * 10. ** 3  # Номинальное напряжение обмотки высшего напряжения
    Ulon = d[u"Ulon"] * 10. ** 3  # Номинальное напряжение обмотки низшего напряжения
    Iid = d[u"Iid"]  # Ток холостого хода трансформатора0
    if (type(Iid) == type(l)):
        flag = 1
        Iid = Iid[0]
    Ush = d[u"Ush"]  # Относительное напряжение короткого замыкания
    if (type(Ush) == type(l)):
        flag = 1
        Ush = Ush[0]
    Ssh = d[u"Ssh"]  # Потери короткого замыкания трансформатора
    if (type(Ssh) == type(l)):
        flag = 1
        Ssh = Ssh[0]

    I_nom_2 = Sn / Ulon  # Номинальный ток во вторичной обмотке
    I_nom_1 = Sn / Uhin  # Номинальный ток в первичной обмотке
    I_xx = I_nom_1 * Iid  # Ток холостого хода
    W_2 = (Ulon / Uhin) * W_1  # Кол-во витков вторичной обмотки
    L_1 = Uhin / (omega * I_xx)  # Индуктивность первичной обмотки
    K = L_1 / W_1 ** 2  # Коэффициент
    I_kz = I_nom_2 / Ush  # Ток короткого замыкания во вторичной обмотке
    R_1 = (Ssh * (W_1 ** 2) ) / (2 * (W_2 ** 2) * (I_kz ** 2) )  # Сопротивление первичной обмотки
    R_2 = Ssh / (2 * (I_kz ** 2) )  # Сопротивление вторичной обмотки
    eta = 1 - (Ush * Uhin) / (L_1 * omega * I_nom_1)  # Потоки рассеивания

    d.clear()
    variables = [eta, R_1, R_2, W_2, K, W_1, Sn]
    variables_name = ['eta', 'R_1', 'R_2', 'W_2', 'K', 'W_1','Sn']
    if (flag):
        for i in range(len(variables)):
            d["["+variables_name[i]+"]"] = variables[i]
    else:
        for i in range(len(variables)):
            d[variables_name[i]] = variables[i]

    return d


a = mc.ld(ur"../db/EnDb.yaml")

for model in a[u"Трансформаторы (2х)"]:
    if (model != u"Марка оборудования"):
        Calculate(a[u"Трансформаторы (2х)"][model])
        print (a[u"Трансформаторы (2х)"][model])
with codecs.open(ur"../db/aaa.yaml", "w", encoding="utf-8") as f:
    yaml.safe_dump(a, f, allow_unicode=1,width = 500)