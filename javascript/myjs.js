var parent = document.getElementById("c1");
var svgNS = "http://www.w3.org/2000/svg";
var Lw=4;
var lv=2;
var movables={};
var mm=4;// pixels / mm
var grid_step=5*mm;
var obj_count=0;
var dvp2len=2*grid_step;// length of binode element
var currentWire=null;

function round_position(x)
{
     return Math.round(x/grid_step)*grid_step;
}

function add_elm(parent,typ,props,content=null)
{
    var elm=document.createElementNS(svgNS,typ);
    for(var key in props)
    {
        elm.setAttributeNS(null,key,props[key]);
    }
    if(content)
    {
        elm.textContent = content;
    }
    parent.appendChild(elm);
    return elm;
}

function set_p(elm,props)
{
    for(var key in props)
    {
        elm.setAttributeNS(null,key,props[key]);
    }
}

function get_p(elm,props)
{
    if(Array.isArray(props))
    {
        var x=[]
        for(var key in props)
        {
            x.push(elm.getAttributeNS(null,key));
        }
        return x;
    }
    else
    {
        return elm.getAttributeNS(null,props);
    }
}

class Ttoolbar
{
    constructor(x,y,w,h) 
    {
        this.x=x;
        this.y=y;
        this.w=w;
        this.h=h;
    }
    add_item()
    {

    }
};

class Tmoveinfo
{
    constructor(grp,obj,x0,y0) 
    {
        this.grp=grp;
        this.obj=obj;
        this.x0 = x0;
        this.y0 = y0;
        this.x = 0;
        this.y = 0;
        this.dx = 0;
        this.dy = 0;
    }
    select(evt)
    {
        console.log("select");
        var selectedElement = evt.target;
        var cls;
        // движемся вверх под дереву объектов пока не найдем graggble
        for(;;)
        {
            cls=get_p(selectedElement,"class");
            if(cls=="draggable")
            {
              break;  
            }
            selectedElement = selectedElement.parentElement;
            if(selectedElement === null)
            {
                return;
            }
        }
        this.grp=selectedElement;
        var key = get_p(this.grp,"id");
        var obj = movables[key];
        if(!obj) {this.grp=null; return;}

        this.x0 = evt.clientX-obj.delta_x;// move relative initial position
        this.y0 = evt.clientY-obj.delta_y;
        this.x = evt.clientX;// move relative initial position
        this.y = evt.clientY;

        this.obj=obj;
//this.grp
        set_p(parent,
        {
            "onmousemove":"mvobj.onmousemove(evt)",
    //        "onmouseout": "mvobj.deselect(evt)",
            "onmouseup": "mvobj.deselect(evt)"
        });
    }

    onmousemove(evt)
    {
        var clX=round_position(evt.clientX);
        var clY=round_position(evt.clientY);
        if(clX != this.x || clY != this.y)
        {
            this.x = clX;
            this.y = clY;
            this.dx = this.x-this.x0;
            this.dy = this.y-this.y0;
            console.log(["mouse",this.dx,this.dy]);
            this.frameID = requestAnimationFrame(this.repaint);
        }
    }

    repaint(evt)
    {
        console.log("xxx");
        set_p(mvobj.grp,{"transform" : "translate(" + mvobj.dx + " " + mvobj.dy + ")"});
    }

    deselect(evt)
    {
      console.log("deselect");
//      cancelAnimationFrame(this.frameID);
      this.obj.delta_x=this.dx;
      this.obj.delta_y=this.dy;
      parent.removeAttributeNS(null, "onmousemove");
    //    this.grp.removeAttributeNS(null, "onmouseout");
      parent.removeAttributeNS(null, "onmouseup");
        this.grp=null;
    }
};

var mvobj= new Tmoveinfo(null,null,0,0);

class Shape 
{
    constructor(id) 
    {
        this.id = id;
        this.delta_x = 0;
        this.delta_y = 0;
        this.index = obj_count;
        movables["id_"+this.index]=this;
        ++obj_count;
    }
}

class Tlamp extends Shape 
{
    constructor(id, x, y) 
    {
        super(id);
        this.x = x;
        this.y = y;
        this.color = ["black", "green"]
        this._on = 0;
    }
}

class RectLamp extends Tlamp 
{
    constructor(id, x, y, w=10, h=10,color_on="green",color_off="black") 
    {
        super(id, x, y);
        this.color=[color_off,color_on];
        this.obj=add_elm(parent,"rect",
        {"id":id,
        "x":x,
        "y":y,
        "width":w,
        "height":h,
        "fill":this.color[this.on],
        "stroke":"none"
        });
    }
    set on(v)  { this._on = v; set_p(this.obj,{"fill":this.color[this.on]});}
    get on()  { return this._on; }
}

function change_state(event,obj)
{
  if(obj._on){obj.on=0;}
  else{obj.on=1;}
}

class CircleLamp extends Tlamp 
{
    constructor(id, x, y, r=10, color_on="green",color_off="black") 
    {
        super(id, x, y);
        this.color=[color_off,color_on];
        this.obj=add_elm(parent,"circle",
        {"id":"mycircle",
        "cx":x,
        "cy":y,
        "r":r,
        "fill":this.color[this.on],
        "stroke":"none"
        });
    }
    set on(v)  { this._on = v; set_p(this.obj,{"fill":this.color[this.on]});}
    get on()  { return this._on; }
}
CircleLamp.name="Круговая лампа"

class Tconnect_point extends Shape
{
    constructor(id,par,parelm, x, y) 
    {
        super(id);
        this.parent=par;
        this.x = x;
        this.y = y;
        this.obj=add_elm(parelm,"circle",
        {"id":id,
        "cx":x,
        "cy":y,
        "r":Lw,
        "fill":"red",
        "stroke":"none"
        });
    }

};
class Tleg extends Shape
{
    constructor(id,par,parelm, x0, y0,x1,y1,color="blue") 
    {
        var x,y;
        super(id);
        this.parent=par;
        x=(x0+x1)/2;
        y=(y0+y1)/2;
        this.x = x;
        this.y = y;
        this.obj=add_elm(parelm,"line",{"stroke":color,"x1":x0,"x2":x1,"y1":y0,"y2":y1});
        this.cp=new Tconnect_point(id,par,parelm,x1,y1);
    }
}

class Trans extends Shape 
{
    constructor(id, x, y) 
    {
        super(id);
        this.x = x;
        this.y = y;
    }
    get R()  { return grid_step*4/Math.sqrt(3); }
    get r()  { return grid_step*3; }

};
Trans.name="трансформатор"

class Trans2 extends Trans
{
    constructor(id, x, y) 
    {
        var phi,x0,y0,obj,c,x0t,y0t;
        super(id, x, y);
        obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.legs=[];
        var names=["Med","Low","High"];
        for(var i=0;i<3;++i)
        {
            phi=2*Math.PI*i/3-Math.PI/3;
            x0=x+this.R*Math.cos(phi);
            y0=y+this.R*Math.sin(phi);
            x0t=x+(this.R+this.r/2)*Math.cos(phi);
            y0t=y+(this.R+this.r/2)*Math.sin(phi);
            c=add_elm(obj,"circle",
            {"cx":x0,
             "cy":y0,
             "r":this.r,
             "fill":"none",
             "stroke":"black",
             "stroke-width":Lw
            });
            add_elm(obj,"text",
            {"x":x0t,
            "y":y0t,
            "text-anchor":"middle",
            "alignment-baseline":"middle"
            },
            names[i]
            );
            if(x0>x)
            {
                this.legs.push(new Tleg(i,this,obj, x0+this.r, y0,x+6*grid_step,y0));
            }
            else
            {
                this.legs.push(new Tleg(i,this,obj, x0-this.r, y0,x-7*grid_step,y0));
            }
        }
    }
}
Trans2.name="трансформатор2"


class RectBtn extends RectLamp
{
    constructor(id, x, y, w=10, h=10,color_on="green",color_off="black") 
    {
        super(id, x, y, w, h,color_on,color_off);
        set_p(this.obj,{"onclick":"change_state(event,this)"});
    }
}
RectBtn.name="прямоугольная кнопка"

class Resistor extends Shape
{
    constructor(id, x, y) 
    {
        super(id, x, y);
        this.legs=[]
        var obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.obj=obj;
        var h=4*mm;
        var w=2*mm;
        add_elm(obj,"rect",
        {"id":id,
        "x":x-w,
        "y":y-h,
        "width":2*w,
        "height":2*h,
        "fill":"white",
        "stroke":"black"
        });
        this.legs.push(new Tleg(0,this,obj, x, y-h,x,y-dvp2len));
        this.legs.push(new Tleg(1,this,obj, x, y+h,x,y+dvp2len));
    }
}
Resistor.name="сопротивление"

class ElecroKey extends Shape
{
    constructor(id, x, y) 
    {
        super(id, x, y);
        this.x=x;
        this.y=y;
        this.legs=[]
        var obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.obj=obj;
        var h=4*mm;
        var w=4*mm;
        this._on=0;
        add_elm(obj,"circle",{"cx":x,"cy":y-h,"r":Lw,"fill":"black","stroke":"none"});
        add_elm(obj,"circle",{"cx":x,"cy":y+h,"r":Lw,"fill":"black","stroke":"none"});
        this.switch = add_elm(obj,"line",{"x1":x+w,"y1":y-h,"x2":x,"y2":y+h,"stroke":"black","stroke-width":Lw});
        this.legs.push(new Tleg(0,this,obj, this.x, this.y-h,x,y-dvp2len));
        this.legs.push(new Tleg(1,this,obj, this.x, this.y+h,x,y+dvp2len));
    }
    set on(v)  
    { 
     this._on = v;
     var w; 
     if(v==1){w=0;}else{w=4*mm;}  
     set_p(this.switch,{"x1":this.x+w});
    }
    get on()  { return this._on; }
}
ElecroKey.name="Выключатель"

class Capasitor extends Shape
{
    constructor(id, x, y) 
    {
        super(id, x, y);
        this.legs=[]
        var obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.obj=obj;
        var h=1*mm;
        var w=4*mm;
        add_elm(obj,"line",{"x1":x-w,"y1":y-h,"x2":x+w,"y2":y-h,"stroke":"black"});
        add_elm(obj,"line",{"x1":x-w,"y1":y+h,"x2":x+w,"y2":y+h,"stroke":"black"});
        this.legs.push(new Tleg(0,this,obj, x, y-h,x,y-dvp2len));
        this.legs.push(new Tleg(1,this,obj, x, y+h,x,y+dvp2len));
    }
}
Capasitor.name="Емкость"

class Inductor extends Shape
{
    constructor(id, x, y) 
    {
        super(id, x, y);
        this.legs=[]
        var obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.obj=obj;
        var h=1*mm;
        var rr=2*mm;
        var dval=["M",x,y-3*rr,
        "A",rr,rr,0,0,1,x,y-1*rr,
        "A",rr,rr,0,0,1,x,y+1*rr,
        "A",rr,rr,0,0,1,x,y+3*rr
        ].join(" ");
        add_elm(obj,"path",{"d":dval,"fill":"none","stroke":"black"});
        this.legs.push(new Tleg(0,this,obj, x, y-3*rr,x,y-dvp2len));
        this.legs.push(new Tleg(1,this,obj, x, y+3*rr,x,y+dvp2len));
    }
}
Capasitor.name="Индуктивность"

class VSource extends Shape
{
    constructor(id, x, y) 
    {
        super(id, x, y);
        this.legs=[]
        var obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.obj=obj;
        var h=1*mm;
        var W=6*mm;
        var w=3*mm;
        add_elm(obj,"line",{"x1":x-W,"y1":y-h,"x2":x+W,"y2":y-h,"stroke":"black"});
        add_elm(obj,"line",{"x1":x-w,"y1":y+h,"x2":x+w,"y2":y+h,"stroke":"black"});
        this.legs.push(new Tleg(0,this,obj, x, y-h,x,y-dvp2len));
        this.legs.push(new Tleg(1,this,obj, x, y+h,x,y+dvp2len));
    }
}
VSource.name="Постоянный источник"

class SinSource extends Shape
{
    constructor(id, x, y) 
    {
        super(id, x, y);
        this.legs=[]
        var obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.obj=obj;
        var R=5*mm;
        var rr=3*mm;
        var dval=[
        "M",x,y,"A",rr,4*rr,0,0,0,x-rr,y,
        "M",x,y,"A",rr,4*rr,0,0,0,x+rr,y        
        ].join(" ");
        add_elm(obj,"circle",{"cx":x,"cy":y,"r":R,"stroke":"black","fill":"none"});
        add_elm(obj,"path",{"d":dval,"fill":"none","stroke":"black"});
        this.legs.push(new Tleg(0,this,obj, x, y-R,x,y-dvp2len));
        this.legs.push(new Tleg(1,this,obj, x, y+R,x,y+dvp2len));
    }
}
SinSource.name="Синусоидальный источник"

class Ground extends Shape
{
    constructor(id, x, y) 
    {
        super(id, x, y);
        this.legs=[]
        var obj=add_elm(parent,"g",{"id":"id_"+this.index,"class":"draggable","transform" : "translate(0 0)",onmousedown:"mvobj.select(evt)"});
        this.obj=obj;
        var h=1.1*mm;
        var w=4*mm;
        add_elm(obj,"line",{"x1":x-w,"y1":y,"x2":x+w,"y2":y,"stroke":"black"});
        w-=1.2*h;
        add_elm(obj,"line",{"x1":x-w,"y1":y+h,"x2":x+w,"y2":y+h,"stroke":"black"});
        w-=1.2*h;
        add_elm(obj,"line",{"x1":x-w,"y1":y+2*h,"x2":x+w,"y2":y+2*h,"stroke":"black"});
        this.legs.push(new Tleg(0,this,obj, x, y,x,y-5*mm));
    }
};
Ground.name="заземление"

class Wire
{
    constructor(x, y)
    {
        this.grp=add_elm(parent,"g",{});
        this.points=[];
        this.last=[x,y];
        this.points.push([x,y]);
        this.segments=[];
    }
    nextPoint(x, y)
    {
        var segment=add_elm(this.grp,"line",{"x1":this.last[0],"y1":this.last[1],"x2":x,"y2":y,"stroke":"black"});
        this.segments.push(segment);
        this.points.push([x,y]);
        this.last=[x,y];
    }
};
Wire.name="заземление"

function CreateWire(evt)
{
  if(evt.altKey)
  {
      console.log("end");
      currentWire=null;
      set_p(parent,{"click":null});
  }
  else
  {
      console.log("point");
      if(currentWire==null)
      {
        currentWire=new Wire(evt.clientX,evt.clientY);
      }
      else
      {
        currentWire.nextPoint(evt.clientX,evt.clientY);
      }
  }
}

function animateLastLineSegment(evt)
{
    
}

//var r1 = new RectLamp("a",50,50,20,50);
//var r2 = new CircleLamp("b",150,50,20);
//var b1 = new RectBtn("c",150,150,20,50);
var lg=new Tleg("some",null,parent,10,10,50,20);
var tr1 = new Trans2("tr1",200,200);
var tr2 = new Trans2("tr2",400,200);
var R=new ElecroKey("r1",50,200);
var C=new SinSource("r1",50,100);
var L=new Inductor("r1",50,150);
set_p(parent,{"onclick":"CreateWire(evt)"});
// http://www.petercollingridge.co.uk/interactive-svg-components/draggable-svg-element