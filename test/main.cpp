#include <sxem.h>
#include <elements.h>
#include <iostream>
#include <iomanip>
#include <src/UnitTest++.h>
#include <cmath>
#include <ode_solver.h>

using namespace std;

const double pipi=3.1415926535897932385;

/*
int main1()
{
    Sxem sx;

    Tnode nd1("node1"), nd2("node2"), nd3("node3"), nd4("node4"), nd5("node5");
    Tresistor p1("r1",2), p2("r2",3);
    Tresistor p8("r8", 1.5); //резистор без эдс, с R=1.5
    Tvsource p7("r7", 2.0, 10.0); //резистор R=2.0 и источником эдс eds=10.0
    Tinductance p6("r6", 7, 5), p5("r5", 8, 6); //индуктивность
    Tcapacitor p3("r3", 3.5), p4("r4", 4.5); //емкость
    nd1 << p2 << p4 << p5 << p8;
    nd2 << p1 << p2 << p3;
    nd3 << p4 << p1 << p6 << p7;
    nd4 << p5 << p6;
    nd5 << p7 << p8;
    nd1 << p3;

//    nd1.maketree();
//    mapcurrents();
//    Sxem::Gs->creatematrix();
//    printmatrix();
    //(*(++sx.elements.begin()))->I.push_back(5);
    //auto& ii = (*(sx.elements.begin()))->I;
    //ii[0] = -ii[0];
    test_currents();

    printsx_y("scheme.dot");
    system("dot -Tpdf scheme.dot -oscheme.pdf");
    return 0;
}*/

SUITE(ELECTRO)
{
TEST(test_update_varlist_0)
{
    Sxem sx;
    const int N=2;
    Tnode n[N];
    Tdvp b[N];
    n[0]<<b[0]<<n[1]<<b[1]<<n[0];
    sx.update_varlist(&n[0]);
    CHECK_EQUAL(b[0].I[0].i,unsigned(0));
    CHECK_EQUAL(b[1].I[0].i,unsigned(0));
    CHECK_EQUAL(b[0].I[0].dir,1);
    CHECK_EQUAL(b[1].I[0].dir,1);
}

TEST(test_update_varlist_1)
{
    Sxem sx;
    const int N=2;
    Tnode n[N];
    Tdvp b[N];
    n[0]<<b[0]<<b[1];
    n[1]<<b[0]<<b[1];
    sx.update_varlist(&n[0]);
    CHECK_EQUAL(b[0].I[0].i,unsigned(0));
    CHECK_EQUAL(b[1].I[0].i,unsigned(0));
    CHECK_EQUAL(b[0].I[0].dir,-1);
    CHECK_EQUAL(b[1].I[0].dir,1);
}


TEST(test_update_varlist_3)
{
    Sxem sx;
    const int N=7;
    Tnode n[N];
    Tdvp b[N];
    int i;

    stringstream so;
    for(i=0;i<N;++i)
    {
     so.seekp(0);
     so<<"node_"<<i;
     n[i].name=so.str();
     so.seekp(0);
     so<<"branch_"<<i;
     b[i].name=so.str();
    }
    n[0] << b[0] << n[1] << b[1] << n[2] << b[2] << n[3] << b[3] << n[4];
    n[2] << b[4];
    n[5] << b[4] << b[5];
    n[6] << b[5] << b[6];
    n[4] << b[6];
    sx.update_varlist(&n[0]);
    sx.i_check();// не должно вылетать исключение
}

TEST(static_sxem)
{
    Sxem sx;
    Tnode ground("ground"), nd("node");
    Tresistor r("r1",1);
    Tvsource  u("u",1);
    ground << r << u;
    nd << r << u;
}

TEST(unlooped_branch)
{
    // висящие ветви
    Sxem sx;
    Tnode ground("ground");
    Tresistor r("r1",1);
    Tvsource  u("u",1);
    ground << r << u;
}

TEST(unlinked_branch)
{
    // несвязная схема
    Sxem sx;
    Tnode ground("ground"), nd("node");
    Tresistor r("r1",1);
    Tvsource  u("u",1);
    ground << r <<r;
    nd << u << u ;
}

//! тест  в которм надо получить 1 ампер через резистор
TEST(do_steps_rsxem)
{
    // create sxem
    Sxem sx;
    Tnode ground("ground"), nd("node");
    Tvsource  u("u",1);
    Tresistor r("r",1);
    //ground << r << u;
    //nd << r << u;
    ground << u << r;
    nd << u << r;
    sx.ground_node=&ground;

    Tode_solver ode(&sx,2e-3,0);
    ode.step();
    float u_res;
    u_res=u.get_U(ode.x.data());
//    cout<<u_res<<endl;
    CHECK_CLOSE(1.,u_res,0.0001);
}

//! аналитическое решение для r l цепочки запитанной синусоидальным источником
class Trl_analitic
{
public:
    double R=1.;//!< сопротивление резистора
    double L=0.05;//!< индуктивность
    double w=2*pipi*50;//!< циклическая частота источника
    double A=1.;//!< амплитуда источника напряжения
    void set_nu(double nu){w=2*pipi*nu;}//!< установка частоты
    double operator()(double t) const
    {
        double L_w=L*w;
        return A*( (L_w/exp((R*t)/L) - L*w*cos(t*w) + R*sin(t*w))/(R*R + L_w*L_w) );
    }
    double source(double t) const
    {
        return sin(w*t);
    }
};


TEST(do_steps_rl)
{
    Trl_analitic analitic;
    Sxem sx;
    Tnode ground("ground"), supply("plus"),uL("uL");
    Tresistor r("r1", analitic.R);
    Tinductance L("L1", analitic.L);
    Tsin_source  u("u",analitic.A);
    ground << u << L;
    supply << u << r;
    uL << r << L;
    sx.ground_node=&ground;

    // integrate
    Tode_solver ode(&sx,2e-3,0);
    double tend = 0.2;
    double t;
    for (t=ode.t(); t<tend; t=ode.step())
    {
        CHECK_CLOSE(analitic(t),-L.get_I(ode.x.data()),0.05);
//        cout<< setw(15)<<t << setw(15)<<analitic.source(t)<<setw(15)<<analitic(t)<<setw(15)<< L.get_I(ode.x.data()) << endl;
    }
}



// TEST(do_steps_trans_KZ)
// {
// }

}

    //union_node(&nd2, &nd);
//    union_node(&nd, &nd2);
//    union_node(&nd1, &ground);

int main()
{
//     do_steps_trans_XX();return 0;
    return UnitTest::RunOneTest("ELECTRO_TRANS","do_steps_trans_KZ");
    //return UnitTest::RunAllTests();
}
