#include <sxem.h>
#include <elements.h>
#include <iostream>
#include <iomanip>
#include <src/UnitTest++.h>
#include <cmath>
#include <ode_solver.h>

using namespace std;

const double pipi=3.1415926535897932385;

SUITE(ELECTRO_TRANS)
{

//! аналитические решения получены в файле analitic_trans.nb
class Ttr2_analitic
{
public:
    float n[2];//!< сопротивление резистора
    float k=0.000001;//!< коэффициент расчета индуктивности L=k*n^2
    float K=0.9;//!< коэффициент передачи магнитного потока
    float L[2];//!< индуктивность
    float R[2];//! сопротивление обмоток
    double w=2*pipi*50;//!< циклическая частота источника
    float I[2];//!< токи обмоток
    float u;//! выходное напряжение
    float A;
    Ttr2_analitic()
    {
        n[0]=300;
        n[1]=30;
        k=0.000001;
        w=2*pipi*50;
        R[0]=1;
        R[1]=0.1;
        A=1;
        for(int i=0;i<2;++i)
        {
            L[i]=k*n[i]*n[i];
        }
    }
    double source(double t) const
    {
        return A*sin(w*t);
    }
};

//!однофазный трансформатор аналитическое решение для режима короткого замыкания
class Ttr2_analitic_KZ:public Ttr2_analitic
{
public:
    Ttr2_analitic_KZ(){R[1]=0.1;}
    void operator()(double t)
    {
        float lam1=-633.0100470637225,lam2=-10.264806737447088;
        I[0]=0.03083068425765504*exp(lam1*t) + 0.03011204074492639*exp(lam2*t) - 0.06094272500258144*cos(w*t) + 0.06310565804579459*sin(w*t);
        I[1]=0.33655020912411593*exp(lam1*t) - 0.027585031754298966*exp(lam2*t) - 0.30896517736981705*cos(w*t) + 0.6772250006825307*sin(w*t);
        u=I[1]*R[1];
    }
};

//!однофазный трансформатор аналитическое решение для режима почти холостого хода
class Ttr2_analitic_XX:public Ttr2_analitic
{
public:
    Ttr2_analitic_XX(){R[1]=10000;}
    void operator()(double t)
    {
        float lam2=-11.111102111637592;
        I[0]=0.03532352246162634*exp(lam2*t) - 0.03532352246597775*cos(w*t) + 0.0012501230338077015*sin(w*t);
        I[1]=-3.179117625590506e-7*exp(lam2*t) + 3.178634135172525e-7*cos(w*t) + 8.988750600297189e-6*sin(w*t);
        u=I[1]*R[1];
    }
};

// аналитические решения получены в файле analitic_trans.nb
TEST(do_steps_trans_XX)
{
    Sxem sx;
    Ttr2_analitic_XX anlc;
    Tnode G("G"), U("U"), U1("U1"), U2("U2");

    Tresistor R1("R1", anlc.R[0]);
    Tresistor R2("R2", anlc.R[1]);
    Tsin_source  gen("u",anlc.A);
    Tr2data trd={anlc.L[0], {300, 30}, anlc.K, {{&G, &U1}, {&G, &U2}}};

    Ttr2 tr("TR1",&trd);
    G << R2 << gen;
    U << gen << R1;
    U1 << R1;
    U2 << R2;

    sx.ground_node=&G;

    // integrate
    Tode_solver ode(&sx,2e-3,0);
    double tend = 0.2;
    double t;
//    ofstream s("trans2.dat");
    for (t=ode.t(); t<tend; t=ode.step())
    {
        anlc(t);
//        s<< setw(15)<<t << setw(15)<<anlc.source(t)<<setw(15)<<anlc.I[0]<<setw(15)<< R1.get_I(ode.x.data()) << endl;
        CHECK_CLOSE(anlc.I[0],R1.get_I(ode.x.data()),0.05);
    }
}

// аналитические решения получены в файле analitic_trans.nb
TEST(do_steps_trans_KZ)
{
    Sxem sx;
    Ttr2_analitic_KZ anlc;
    Tnode G("G"), U("U"), U1("U1"), U2("U2");

    Tresistor R1("R1", anlc.R[0]);
    Tresistor R2("R2", anlc.R[1]);
    Tsin_source  gen("u",anlc.A);
    Tr2data trd={anlc.L[0], {300, 30}, anlc.K, {{&G, &U1}, {&G, &U2}}};

    Ttr2 tr("TR1",&trd);
    G << R2 << gen;
    U << gen << R1;
    U1 << R1;
    U2 << R2;

    sx.ground_node=&G;

    // integrate
    Tode_solver ode(&sx,2e-3,0);
    double tend = 0.2;
    double t;
    ofstream s("trans2.dat");
    for (t=ode.t(); t<tend; t=ode.step())
    {
        anlc(t);
        s<< setw(15)<<t << setw(15)<<anlc.source(t)<<setw(15)<<anlc.I[1]<<setw(15)<< R2.get_I(ode.x.data()) << endl;
//        CHECK_CLOSE(anlc.I[1],R2.get_I(ode.x.data()),0.05);
    }
}

}