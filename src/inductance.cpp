#include <inductance.h>

using namespace std;

//Класс индуктивности
Tinductance::Tinductance(std::string name_, float L_): Tdvp(name_), L(L_) {}

void Tinductance::upd_mtr(Tupd_data<double>& d) const
{
    float elm =(float)( 2. * L / d.dt);
    for (auto idsc : I)
    {
         d.matr(index,idsc.i)= (idsc.dir==1 ? elm : -elm);
    }
    int i;
    i=pol[0]->index;
    if(i>=0){d.matr(index,i)=-1;}
    i=pol[1]->index;
    if(i>=0){d.matr(index,i)=1;}
    /*
    float elm =(float)( d.alpha * L);
    for (auto i : I)
    {
        **d.mtr = elm * i.dir; ++d.mtr;
    }
    **d.mtr = elm; ++d.mtr; //0
    **d.mtr = -elm; ++d.mtr; //1
    */
}

void Tinductance::upd_q(Tupd_data<double>& d) const
{
    float ii=0,res;
    for (auto idsc : I)
    {
         ii+=d.x0[idsc.i]*idsc.dir;
    }
    res= ii * 2. * L / d.dt;
    int i;
    i=pol[0]->index;
    if(i>=0){res+=d.x0[i];}
    i=pol[1]->index;
    if(i>=0){res-=d.x0[i];}
    d.q[index] = res;

    // float ii, uu;
    // ii = get_I(d.x0);
    // uu = get_U(d.x0);
    // d.q[index] = L * d.alpha * ii + uu;
}
