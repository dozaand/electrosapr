#pragma once
#include <dvp.h>
#include <sxem.h>

class Tinductance : public Tdvp //класс индуктивности
{
public:
    float L; //индуктивность катушки
    Tinductance(): Tdvp(""), L(1.) {}
    Tinductance(std::string name_, float L_);
    virtual void upd_mtr(Tupd_data<double>& d) const;
    virtual void upd_q(Tupd_data<double>& d) const;
};
