#pragma once
#include <dvp.h>

class Tel_key : public Tdvp //класс резистора-источника эдс
{
public:
    float r = 0; //сопротивление
    float eds = 0; //ЭДС
    Tel_key(std::string name_, float r_);
    Tel_key(std::string name_, float r_, float eds_);
//    virtual void addinmatrix(Tsxem_solver* Gs);
//    virtual void upd_mtr(float*& p) const;
//    virtual void upd_q(float* q) const;
};
