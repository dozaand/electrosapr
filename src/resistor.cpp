#include <resistor.h>
#include <sxem.h>
#include <node.h>
#include <vector>
#include <cmath>

using namespace std;

//класс резистора-источника эдс
Tresistor::Tresistor(std::string name_, float r_): Tdvp(name_), r(r_) {}

void Tresistor::upd_mtr(Tupd_data<double>& d) const
{
    for (auto idsc : I)
    {
         d.matr(index,idsc.i) = (idsc.dir==1 ? r : -r);
    }
    int i;
    i=pol[0]->index;
    if(i>=0){d.matr(index,i)=-1;}
    i=pol[1]->index;
    if(i>=0){d.matr(index,i)=1;}
    /*
    for (auto i : I)
    {
        **d.mtr = r * i.dir; ++d.mtr;
    }
    **d.mtr = 1.; ++d.mtr; //0
    **d.mtr = -1.; ++d.mtr; //1
    */
}

void Tresistor::upd_q(Tupd_data<double>& d) const
{
    float ii=0,res;
    for (auto idsc : I)
    {
         ii+=d.x0[idsc.i]*idsc.dir;
    }
    int i;
    res=-ii*r;
    i=pol[0]->index;
    if(i>=0){res+=d.x0[i];}
    i=pol[1]->index;
    if(i>=0){res-=d.x0[i];}
    d.q[index] = res;
}
