#pragma once

#include <dvp.h>
#include <sxem.h>
#include <inductance.h>

//! данные для инициализации однофазного трансформатора
class Tr2data
{
public:
    float L1;//!< индуктивность первичной обмотки
    float w[2];//!< эффективное число витков (первичной/вотричной) обмоки
    float K;//!< коэффициент потерь магнитного потока [0-1]
    Tnode* legs[2][2];//!< индексация: [первичная/вторичная][номер ноги]
};

//! данные для инициализации трехфазного трансформатора
class Tr3data
{
public:
    float L1;//!< индуктивность первичной обмотки
    float w[2];//!< эффективное число витков (первичной/вотричной) обмоки
    float K;//!< коэффициент потерь магнитного потока [0-1]
    Tnode* legs[2][3][2];//!< индексация: [первичная/вторичная][номер фазы][номер ноги]
};

class Ttr2
{
    float K;
    float M;
    Tinductance L[2];
public:
    Ttr2(std::string name_, Tr2data* d)
    {
        float kl;//коэффициент характеризующий объем магнитопровода L = kl w^2
        L[0].L = d->L1;
        kl = d->L1 / (d->w[0] * d->w[0]);
        L[1].L = (d->w[1] * d->w[1]) * kl;
        M = (d->w[0] * d->w[1]) * kl * d->K;
        // втыкаем ноги
        for (int i = 0; i < 2; ++i)
        {
            *(d->legs[i][0]) << L[i];
            *(d->legs[i][1]) << L[i];
        }
    }
    virtual void upd_mtr(Tupd_data<double>& d) const;
    virtual void upd_q(Tupd_data<double>& d) const;
};
