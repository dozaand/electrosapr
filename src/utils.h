#include <fstream>
#include <dvp.h>

void printmatrix();
void make_narrow(std::ofstream& s, Tdvp* current_element, std::string color, std::string style);
void printsx_y(const char* name);
void test_currents();
void union_node(Tnode * n1, Tnode * n2);