#include <sxem.h>
#include <node.h>
#include <vector>
#include <cmath>
#include <el_key.h>

using namespace std;

//класс резистора-источника эдс
Tel_key::Tel_key(std::string name_, float r_): Tdvp(name_)
{
    r = r_;
}
Tel_key::Tel_key(std::string name_, float r_, float eds_): Tdvp(name_)
{
    r = r_;
    eds = eds_;
}