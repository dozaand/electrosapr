#pragma once
#include <matrix3_2/inc/matr_umf.h>

template <class T>
class Tupd_data
{
public:
    Tupd_data(matr_umf<double>& matr_):matr(matr_){}
    matr_umf<double>& matr;
    double t;//! модельное время
    T alpha;//! параметр аналогичный dt 2/dt
    T* x0;//! решение в предидущий момент времени
	T dt;

    T* q;//! правая часть которую надо модифицировать
    T** mtr;//! указатель на начало перечня ссылок на данные матрицы в порядке обхода матрицы
};