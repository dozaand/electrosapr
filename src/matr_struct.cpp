#include <matr_struct.h>

using namespace std;
    //! заполнение матрицы в CRF формате и заполнение массива указателей для последовательного доступа к матричным элементам
void Tmatr_struct::to_crf(std::vector<double>& Ax, std::vector<int>& Ap, std::vector<int>& Ai, std::vector<double*>& pAx)
//void Tmatr_struct::to_crf(std::vector<float>& Ax, std::vector<int>& Ap, std::vector<int>& Ai, std::vector<float*>& pAx)
{
	int i, j, row_pos;
	dim = max_i + 1;
    pAx.resize(pos);
    Ax.resize(nnz); Ax0 = Ax.data();
    Ai.resize(nnz);
    Ap.resize(dim+1);
	vector<int> row_index(dim);
    for(i=0;i<dim;++i)row_index[i]=0;
	for (auto it : data)
	{
        j = it.first.first;
		i = it.first.second;
		if (i >= 0)
		{
			++row_index[j];
		}
	}
	Ap[0] = 0;
	for (i = 1; i < dim+1; ++i){ Ap[i ] = Ap[i-1] + row_index[i-1];}// посчитали старты строк
	for (i = 0; i < dim; ++i){ row_index[i] = Ap[i]; }// установили текущие позиции для записи элементов строки
//	for (i = 0; i < Ax.size(); ++i){ Ax[i] = i+1; }
	for (auto it : data)
	{// если у строки неотрицательный индекс то все ок - добавляем указатель на матрицу иначе указываем на левый ноль
        j=it.first.first;
        i=it.first.second;
		if (i >= 0)
		{
			row_pos = row_index[j];
			pAx[it.second] = &Ax[row_pos];
			Ai[row_pos] = i;
			++row_index[j];
		}
		else
		{
			pAx[it.second] = &zero;
		}
	}
}
#include <iostream>
#include <iomanip>
using namespace std;
void Tmatr_struct::pt() const
{
/* cout<<"pos : "<<pos<<endl;
 cout<<"max_i : "<<max_i<<endl;
 cout<<"max_j : "<<max_j<<endl;
 for(auto i:data)
 {
    cout<<" - "<<i.j<<" "<<i.i<<endl;
 }*/
}
