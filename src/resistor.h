#pragma once
#include <dvp.h>
#include <sxem.h>

class Tresistor : public Tdvp //класс резистора-источника эдс
{
public:
    float r = 0; //сопротивление
    Tresistor(std::string name_, float r_);
//    virtual void addinmatrix(Tsxem_solver* Gs);
    virtual void upd_mtr(Tupd_data<double>& d) const;
    virtual void upd_q(Tupd_data<double>& d) const;
};
