#include <node.h>
#include <dvp.h>
#include <sxem.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>

using namespace std;

Sxem* Sxem::Gs = nullptr;

// Решил собрать две функции в одну. Построение дерева уж очень простое
// для больших деревьев проход до корня уж очень затратен, решил сделать другую логику с контролем расстояния до корня тогда завершаем проход кода предшественник один итотже ужел
int Sxem::update_varlist(Tnode* ground_node_)
{
    if(ground_node_!=nullptr)
    {
        ground_node = ground_node_;
    }
    if(ground_node==nullptr)
    {
        throw "ground not initialised";
    }
    for (auto& i : nodes) { i->level = -1; } //чистим узлы
    nodes.erase(ground_node);
    ground_node->maketree(0);
    Iindex index;
    index.i = 0;
    // а теперь делаем токи
    for (auto& dvp : elements)
    {
        // если встретили ветвь не на дереве - замыкаем цикл
        if (dvp->tree_direction() == 0)
        {
            Tnode* node0 = dvp->pol[0], *node1 = dvp->pol[1];
            Tdvp* element;
            index.dir = 1;
            dvp->I.push_back(index);
            int level;
            for (level = max(node0->level, node1->level); node0 != node1; --level)
            {
                if (node0->level == level)
                {
                    element = node0->elparent;
                    index.dir = element->tree_direction();
                    element->I.push_back(index);
                    node0 = node0->parent;
                }
                if (node1->level == level)
                {
                    element = node1->elparent;
                    index.dir = -element->tree_direction();
                    element->I.push_back(index);
                    node1 = node1->parent;
                }
            }
            ++index.i;
        }
    }
    numberI = index.i;
    // нумеруем узлы (при этом выкинули земляной узел) и отступили на количество независимых токов
    int i = numberI;
    for (auto& node : nodes) {node->index = i; ++i;}
    size_=i;
    // нумеруем ветви
    i = 0; for (auto& elm : elements) {elm->index = i; ++i;}
    ground_node->index = -1;
    return size_;
}

bool v_non_zero(vector<int>& v)
{
    for (int i : v) {if (i != 0) return false;}
    return true;
}

void Sxem::i_check() const
{
    vector<int> icurr(numberI);
    vector<Tnode*> fail_nodes;
    for (const auto& node : nodes)
    {
        for (const auto& element : elements)
        {
            int dir = element->tree_direction();
            for (auto i : element->I)
            {
                icurr[i.i] += i.dir * dir;
            }
        }
        if (v_non_zero(icurr)) {fail_nodes.push_back(node);}
        fill(icurr.begin(), icurr.end(), 0);
    }
    if (!fail_nodes.empty()) {throw fail_nodes;}
}

void Sxem::save_sxem(const char* file_name) const
{

}
void Sxem::load_sxem(const char* file_name) const
{

}
void Sxem::save_sxem_state(const char* file_name) const
{

}
void Sxem::load_sxem_state(const char* file_name) const
{

}


template <class T>
void ptsome(T* el)
{
    if (el == nullptr) {cout << "nullptr";}
    else {cout << el->name;}
}

void ptnode(const Tnode* nd)
{
    if (nd)
    {
        cout << nd->name << endl;
        cout << "      parent : ";
        ptsome(nd->parent);
        cout << endl;
        cout << "      elparent : ";
        ptsome(nd->elparent);
        cout << endl;
        cout << "      level : ";
        cout << nd->level;
        cout << endl;
        cout << "   connected:\n{\n";
        for (auto& element : nd->els)
        {
            cout << "     -";
            ptsome(element);
            cout << endl;
        }
        cout << "}\n";
    }
    else
    {
        cout << "nullptr\n";
    }
}

void Sxem::pt() const
{
    cout << "=======nodes==========\n";
    ptnode(ground_node);
    for (auto& nd : nodes)
    {
        ptnode(nd);
    }
    cout << "=======branches==========\n";
    for (auto& element : elements)
    {
        cout << "-";
        ptsome(element);
        cout << " { ";
        ptsome(element->pol[0]);
        cout << " , ";
        ptsome(element->pol[1]);
        cout << " }  I:";
        for (auto ind : element->I) {cout << ind.i << " " << ind.dir << " | ";}
        cout << endl;
    }
}

void Sxem::create_matrix(Tmatr_struct& mtr)
{
    for(auto& element : elements)
    {
        element->s_mtr(mtr);
    }
}
void Sxem::update_matrix(Tupd_data<double>& d)
{
    for(auto& element : elements)
    {
        element->upd_mtr(d);
    }
}
void Sxem::update_q(Tupd_data<double>& d)
{
	for (Tdvp* element : elements)
    {
        element->upd_q(d);
    }
}
