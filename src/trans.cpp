#include <trans.h>

void Ttr2::upd_mtr(Tupd_data<double>& d) const
{
    float M_dt = 2. * M / d.dt;
    int i,j;
    for (i = 0; i < 2; ++i)
    {
        L[i].upd_mtr(d);
        j=!i;
        for (auto idsc : L[i].I)
        {
            d.matr(L[j].index, idsc.i) = (idsc.dir == 1 ? -M_dt : M_dt);
        }
    }
}
void Ttr2::upd_q(Tupd_data<double>& d) const
{
    L[0].upd_q(d);
    L[1].upd_q(d);
}
