#include <dvp.h>
#include <node.h>
#include <vector>

bool Tnode::is_root() const
{
    return level == 0;
//    return parent == this || parent == nullptr;
}

bool Tnode::set_parent(Tnode* target, Tdvp* el)
{
    if (target->parent) return false;
    else
    {
        target->parent = this;
        target->elparent = el;
        return true;
    }
}

void Tnode::maketree(int level_)
{
    Tnode* next_node;
    if (parent == nullptr) { parent = this; }
    level = level_;
    std::vector<Tnode*> next;
    for (auto& i : els)
    {
        next_node = i->next_node(this);
        if (set_parent(next_node, i))
        {
            next.push_back(next_node);
            //next_node->maketree(level+1);
        }
    }
    for (auto j : next)
    {
        j->maketree(level + 1);
    }
}

Tdvp * last_element = nullptr;

Tnode& operator<<(Tnode& node, Tdvp& el)
{
    last_element = &el;
    if (el.pol[0] != nullptr)
    {
        el.pol[1] = &node;
    }
    else { el.pol[0] = &node; }

    node.els.insert(&el);

    return node;
}

Tnode& operator<<(Tnode& node1, Tnode& node2)
{
    if (last_element)
        return node2 << (*last_element);
    else
        return node2;
}

//!���������� ����� -> true; ��������� ����� -> false
int Tnode::is_outcoming(Tdvp* current_element)
{
    if (current_element->pol[0] == this)
    {
        return 1;
    }
    else
    {
        if (current_element->pol[1] == this) return 0;
        else throw "dvp is not incoming or oucoming";
    }
}