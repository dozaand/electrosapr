#pragma once
#include <dvp.h>
//#include <sxem.h>

//!Класс емкости
class Tcapacitor : public Tdvp
{
public:
    float C = 0; //!< емкость конденсатора
    Tcapacitor(std::string name_, float C_);
    virtual void upd_mtr(Tupd_data<double>& d) const;
    virtual void upd_q(Tupd_data<double>& d) const;
};
