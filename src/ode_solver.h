#pragma once
#include <vector>
#include <upd_data.h>
#include <sxem.h>

class Tode_solver
{
    int dim;
    Sxem* sx;
    matr_umf<double> aB_p_A;//!< alpha*B + A для метода трапеций решаем уравнение (2*B/dt + A) x_{k+1} = (2*B/dt - A) x_k - 2 q
    matr_umf_lu<double> LU_aB_p_A;
    Tupd_data<double> d;
    vector<double> q;
public:
    vector<double> x;

    Tode_solver(Sxem* sx_,double dt= 2e-3,double t0=0);
    void set_dt(double dt);
    double step();
    double t() const {return d.t;}
};
