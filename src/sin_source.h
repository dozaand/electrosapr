#pragma once
#include <dvp.h>
#include <sxem.h>

class Tsin_source : public Tdvp //класс резистора-источника эдс
{
public:
    float eds; //!<ЭДС
    float omega;//!< частота
    float fi0;//!< начальная фаза
    Tsin_source(std::string name_, float eds_, float omega_=6.2831853071795864769*50, float fi0_=0);
    virtual void s_mtr(Tmatr_struct& S) const;
    virtual void upd_mtr(Tupd_data<double>& d) const;
    virtual void upd_q(Tupd_data<double>& d) const;
};
