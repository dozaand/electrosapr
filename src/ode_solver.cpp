#include <ode_solver.h>

Tode_solver::Tode_solver(Sxem* sx_,double dt,double t0):
dim(sx_->update_varlist(sx_->ground_node)),
sx(sx_),
aB_p_A(dim),
LU_aB_p_A(dim),
d(aB_p_A),
q(dim),
x(dim)
{
    for (int i = 0; i < dim; ++i){ x[i] = 0; }
    set_dt(dt);
    d.t = t0;//! модельное время
    sx->update_matrix(d);//!< задание структуры и значений
//  pt(aB_p_A);
    aB_p_A.to_crf();
    d.x0 = x.data();//! решение в предидущий момент времени
    d.q = q.data();//! правая часть которую надо модифицировать
}
void Tode_solver::set_dt(double dt)
{
    d.alpha = 2. / dt;//! параметр аналогичный dt
    d.dt = dt;
}
double Tode_solver::step()
{
    sx->update_matrix(d);
    sx->update_q(d);
/*    {
      ofstream s("mtr.dat");
      int i,dim;
      dim=aB_p_A.Ax.size();
      for(i=0;i<dim;++i) {s<<setw(15)<<aB_p_A.Ax[i];}
      s<<endl;
      dim=aB_p_A.Ai.size();
      for(i=0;i<dim;++i) {s<<setw(15)<<aB_p_A.Ai[i];}
      s<<endl;
      dim=aB_p_A.Ap.size();
      for(i=0;i<dim;++i) {s<<setw(15)<<aB_p_A.Ap[i];}
      s<<endl;
      dim=q.size();
      for(i=0;i<dim;++i) {s<<setw(15)<<q[i];}
      s<<endl;
    }*/
    LU_aB_p_A.update(aB_p_A);
    LU_aB_p_A.mul(q.data(), x.data());
    d.t+=d.dt;
    return d.t;
}
