#include <vsource.h>
#include <sxem.h>
#include <node.h>
#include <vector>
#include <cmath>

using namespace std;

//класс резистора-источника эдс
Tvsource::Tvsource(std::string name_, float eds_): Tdvp(name_), eds(eds_) {}

void Tvsource::s_mtr(Tmatr_struct& S) const
{
	S.next(index, pol[0]->index);
	S.next(index, pol[1]->index);
}


void Tvsource::upd_mtr(Tupd_data<double>& d) const
{
    int i;
    i=pol[0]->index;
    if(i>=0){d.matr(index,i)=-1;}
    i=pol[1]->index;
    if(i>=0){d.matr(index,i)=1;}

/*    **d.mtr = -1; ++d.mtr; //0
    **d.mtr = 1; ++d.mtr; //1*/
}

void Tvsource::upd_q(Tupd_data<double>& d) const
{
    float res = eds;
    res = eds;
    int i;
    i=pol[0]->index;
    if(i>=0){res+=d.x0[i];}
    i=pol[1]->index;
    if(i>=0){res-=d.x0[i];}
    d.q[index]=res;
//    d.q[index] = eds;
}
