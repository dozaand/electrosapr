#pragma once
#include <string>
#include <vector>
#include <string>
#include <sxem.h>
#include <iostream>

class Tnode
{
public:
    Tnode* parent = nullptr; //!указатель на нод родитель
    Tdvp* elparent = nullptr; //!указатель на элемент, соединяющий с пэрентом
    int level = -1; //! уровень в дереве
    int index;
    std::string name;
    std::set<Tdvp*> els;
    Tnode() { Sxem::Gs->nodes.insert(this); }
    Tnode(std::string name_) : name(name_) { Sxem::Gs->nodes.insert(this); }
    bool is_root() const;
    bool set_parent(Tnode* target, Tdvp* el);
    void maketree(int level);
    int is_outcoming(Tdvp* current_element);
    float get_U(float* x) const {return x[index];}
    void del_node () { Sxem::Gs->nodes.erase(Sxem::Gs->nodes.find(this)); } //удаление иэтого нода из глобальной схемы
};

Tnode& operator << (Tnode& node, Tdvp& el);
Tnode& operator << (Tnode& node1, Tnode& node2);
