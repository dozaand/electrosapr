#include <vector>
#include <node.h>
#include <sxem.h>
#include <dvp.h>
#include <fstream>
#include <cmath>

using namespace std;


void printmatrix()
{
    for (auto& i : Sxem::Gs->matrix)
    {
        for (auto& j : i)
        {
            cout << j << '\t';
        }
        cout << '\n';
    }
}



void make_narrow(ofstream& s, Tdvp* current_element, string color, string style)
{
    int a = 0, b = 1;
    //if (current_element->pol[1]->parent != current_element->pol[0]){ swap(a, b); style = "dotted"; }
    if (color != "black" && current_element->pol[1]->parent != current_element->pol[0]) { swap(a, b); style = "dotted"; }
    s << current_element->pol[a]->name.c_str() << "->" << current_element->pol[b]->name.c_str() << "[label=\"";
    s << current_element->name.c_str() << " ";
    for (auto& j : current_element->I)
    {
        if (j.dir < 0)
        {
            s << " " << "-I" << j.i;
        }
        else
        {
            s << " " << "I" << j.i;
        }
    }
    s << "\",";
    s << "color=" << color << ",";
    s << "style=" << style << "];\n";
}

void printsx_y(const char* name)
{
    ofstream s(name);
    s << "digraph G {\n";
    // for (auto& i : Sxem::Gs->elements)
    // {
    // if (Sxem::Gs->tree_elements.find(i) != Sxem::Gs->tree_elements.end())
    // {
    //     make_narrow(s, i, "red", "none");
    // }
    // else
    // {
    //     make_narrow(s, i, "black", "none");
    // }
    // }
    s << "}\n";
}
//!���� ���� ����������� ��������� ��������� "currents are true", ���������� ������ ����� � ����� ���� ������
void test_currents()
{
    vector<int> currents(Sxem::Gs->numberI, 0);
    int test = 0;
    bool flag = true;
    for (auto& j : Sxem::Gs->nodes)
    {
        for (auto& k : j->els)
        {
            if (j->is_outcoming(k))
            {
                for (auto& i : k->I)
                {
                    currents[i.i] += i.dir;
                }
            }
            else
            {
                for (auto& i : k->I)
                {
                    currents[i.i] -= i.dir;
                }
            }
        }
        for (auto& i : currents)
        {
            test += i;
            if (test)
            {
                cout << "current I" << i << " " << "uncorrect for " << j->name.c_str() << "\n";
                test = 0;
                flag = false;
            }
        }
        if (flag)
        {
            cout << "currents are true for " << j->name.c_str() << "\n";
        }
        cout << "\n";
        flag = true;
    }
    if (flag)
    {
        cout << "currents are true \n";
    }
    cout << "\n";
}


void union_node(Tnode * n1, Tnode * n2)
{
    //n1 - �������, n2 - ��������� � �������� ��� ��� �������� �� n1
    //parent-�� ��� ���, �.�. ��� ��� ������
    for (auto& i : n1->els)
    {
        if (i->pol[0] == n1) { i->pol[0] = n2; }
        else { i->pol[1] = n2; }
    }
    n1->del_node();
    //delete n1;
}