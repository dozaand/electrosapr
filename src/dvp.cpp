#include <dvp.h>
#include <node.h>
#include <sxem.h>
#include <math.h>
#include <cmath>

using namespace std;


Tdvp::Tdvp()
{
    pol[0] = nullptr; pol[1] = nullptr;
    Sxem::Gs->elements.insert(this);
}

Tdvp::Tdvp(std::string name_)
{
    name = name_;
    pol[0] = nullptr; pol[1] = nullptr;
    Sxem::Gs->elements.insert(this);
}
Tdvp::~Tdvp(){}

void Tdvp::push_node(Tnode * n)
{
    if (pol[0])
    {
        if (pol[1])throw "qqq";
        pol[1] = n;
    }
    else
    {
        pol[0] = n;
    }
}

void Tdvp::s_mtr(Tmatr_struct& S) const
{
    for (auto i : I)
    {
      S.next(index, i.i);
    }
    S.next(index, pol[0]->index);
    S.next(index, pol[1]->index);
}

float Tdvp::get_I(const double* x) const
{
    float res = 0;
    for (auto i : I)
    {
        if (i.dir == 1) res +=float(x[i.i]);
		else res -= float(x[i.i]);
    }
    return res;
}

float Tdvp::get_U(const double* x) const
{
    int i;
    float res;
    i = pol[1]->index;
    res = float((i >= 0 ? x[i] : 0));
    i = pol[0]->index;
    res -=float( (i >= 0 ? x[i] : 0));
    return res;
}


Tnode* Tdvp::next_node(const Tnode* p) const { return p == pol[0] ? pol[1] : pol[0]; }



