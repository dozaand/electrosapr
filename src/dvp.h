#pragma once
#include <string>
#include <vector>
#include <node.h>
#include <matr_struct.h>
#include <upd_data.h>

//! класс для хранения индекса тока и его направления
class Iindex
{
public:
    unsigned i: 30; // индекс тока
    int dir: 2; // 1 если прямое направление и -1 если обратное
};

//Класс двухполюсника
class Tdvp
{
public:
    //! массив индексов токов протекающих через данный двухполюсник плюс направления этих токов
    std::vector<Iindex> I;//!< if I={1,5,-6} then ток ветви равен I[1]+I[5]-I[6];
    //! имя двухполюсника
    std::string name;
    Tdvp();
    Tdvp(std::string name_);
    virtual ~Tdvp();
    void push_node(Tnode * n);
    Tnode* pol[2];//!< контактные полщадки двухполюсника
    Tnode* next_node(const Tnode* p) const;
    //! 1 если совпадает с направлением роста дерева -1 если противоположно 0 если ветвь не на дереве
    int tree_direction() const
    {
        if (pol[1]->elparent == this) {return 1;}
        else if (pol[0]->elparent == this) {return -1;}
        else return 0;
    }
    virtual void s_mtr(Tmatr_struct& S) const;
    virtual void upd_mtr(Tupd_data<double>& d) const {};
    virtual void upd_q(Tupd_data<double>& d) const {};
    int index;// Номер ветви
    //! расчет суппарного тока в ветви
    float get_I(const double* x) const;
    //! расчет напряжения приложенного к ветви
    float get_U(const double* x) const;
};



//signum-function
//inline float sgn(int val) { return (val > 0) ? (float(1)) : ((val < 0) ? (float(-1)) : (float(0))); }
template <typename T> int sgn(T val)
{
    return (T(0) < val) - (val < T(0));
}

