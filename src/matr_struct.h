#pragma once
#include <vector>
#include <map>
#include <utility>
//#include <multimap>

// класс для запоминания порядка обхода элементов матрицы
class Tij
{
public:
	int j;
	int i;
};

class Tmatr_struct
{
	std::map<std::pair<int, int>, int> data;
//	std::vector<Tij> data; //!< Для каждой строки для каждого индекса указываем перечень номеров акий обращения к элементу
//	std::map<int, std::map<int, int > > data; //!< Для каждой строки для каждого индекса указываем перечень номеров акий обращения к элементу
    int pos=0;
    int max_i=0;
    int max_j=0;
    int nnz=0;
	int dim;
public:
    double* Ax0;
    void flush() {data.clear(); pos = max_i = max_j = 0;}
    void next(int j, int i)
    {
		data[std::make_pair(j,i)]=pos;
        if(i>=0 && j>=0){++nnz;}
        if (i > max_i) max_i = i;
        if (j > max_j) max_j = j;
        ++pos;
    }
    int NNZ() const {return nnz;}
    int size() const {return max_i+1;}
    double zero = 0;
    //! заполнение матрицы в CRF формате и заполнение массива указателей для последовательного доступа к матричным элементам
    void to_crf(std::vector<double>& Ax, std::vector<int>& Ap, std::vector<int>& Ai, std::vector<double*>& pAx);
    void pt() const;
};

