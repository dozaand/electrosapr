#pragma once
#include <dvp.h>
#include <sxem.h>

class Tvsource : public Tdvp //класс резистора-источника эдс
{
public:
    float eds = 0; //ЭДС
    Tvsource(std::string name_, float eds_);
//    virtual void addinmatrix(Tsxem_solver* Gs);
	virtual void s_mtr(Tmatr_struct& S) const;
    virtual void upd_mtr(Tupd_data<double>& d) const;
    virtual void upd_q(Tupd_data<double>& d) const;
};
