#include <sin_source.h>
#include <cmath>

using namespace std;

//класс резистора-источника эдс
Tsin_source::Tsin_source(std::string name_, float eds_, float omega_, float fi0_): Tdvp(name_), eds(eds_), omega(omega_), fi0(fi0_) {}

void Tsin_source::s_mtr(Tmatr_struct& S) const
{
    S.next(index, pol[0]->index);
    S.next(index, pol[1]->index);
}


void Tsin_source::upd_mtr(Tupd_data<double>& d) const
{
    int i;
    i=pol[0]->index;
    if(i>=0){d.matr(index,i)=-1;}
    i=pol[1]->index;
    if(i>=0){d.matr(index,i)=1;}

    /*
    **d.mtr = -1; ++d.mtr; //0
    **d.mtr = 1; ++d.mtr; //1
    */
}

void Tsin_source::upd_q(Tupd_data<double>& d) const
{
    float t0=omega * d.t + fi0;
    float res;
	res = eds * (sin(t0) + sin(t0+omega * d.dt));
    int i;
    i=pol[0]->index;
    if(i>=0){res+=d.x0[i];}
    i=pol[1]->index;
    if(i>=0){res-=d.x0[i];}
    d.q[index]=res;
}
