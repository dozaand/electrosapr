#include <capacitor.h>

using namespace std;


Tcapacitor::Tcapacitor(string name_, float C_): Tdvp(name_), C(C_) {}

void Tcapacitor::upd_mtr(Tupd_data<double>& d) const
{
    float elm =(float)( 2. * C / d.dt);
    for (auto idsc : I)
    {
         d.matr(index,idsc.i)= (idsc.dir==1 ? 1 : -1);
    }
    int i;
    i=pol[0]->index;
    if(i>=0){d.matr(index,i)=-elm;}
    i=pol[1]->index;
    if(i>=0){d.matr(index,i)=elm;}

/*    float elm = float(d.alpha * C);
    for (auto i : I)
    {
        **d.mtr = -i.dir; ++d.mtr;
    }
    **d.mtr = -elm; ++d.mtr; //0
    **d.mtr = elm; ++d.mtr; //1*/
}

void Tcapacitor::upd_q(Tupd_data<double>& d) const
{
    float elm =(float)( 2. * C / d.dt);
    float ii=0,res;
    for (auto idsc : I)
    {
         ii+=d.x0[idsc.i]*idsc.dir;
    }
    res = ii;
    int i;
    i=pol[0]->index;
    if(i>=0){res+=elm*d.x0[i];}
    i=pol[1]->index;
    if(i>=0){res-=elm*d.x0[i];}
    d.q[index] = res;
/*    float ii, uu;
    ii = get_I(d.x0);
    uu = get_U(d.x0);
    d.q[index] = C * d.alpha * uu + ii;*/
}
