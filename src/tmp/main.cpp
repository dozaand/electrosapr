#include <iostream>
#include  <sxem.h>

using namespace std;

int main()
{
    Tsxem sx;
    Twire grnd,wsourse,w1;
    Tinductor L;
    Tresiztor R;
    Tusource U;

    {
        Tthis_sx sxscope(&sx);

        grnd << U(0) << L(0);
        wsourse << R(0);
        w1 << R(1) << L(1);
    }

    return 0;
}