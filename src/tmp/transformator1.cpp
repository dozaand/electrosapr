#include <transformator1.h>
// Q+A.dy/dt+B*Y;
void Ttrans2::update_matrix()
{
    float Smu=S*mu;
    A[0][0] = Smu*w[0]*w[0];
    A[0][1] = Smu*w[0]*w[1]*M;
    A[1][0] = Smu*w[1]*w[0]*M;
    A[1][1] = Smu*w[1]*w[1]*M;

    B[0][0] = -R[0];
    B[0][1] = 0;
    B[1][0] = 0;
    B[1][1] = -R[1];
}
