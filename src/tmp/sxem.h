#pragma once
#include <vector>
#include <map>
#include <array>
#include <set>
#include <sapr_element.h>

using namespace std;
class Tsxem_element;
class Twire;

class Tsxem
{
public:
    ~Tsxem();
    set<Tsxem_element*> elements;
    // set<Tlogical_element*> logicals_to_update;
    vector<Tsxem_element*> inner_elements;
    set<Twire*> links_to_update;
    void update();
    // фабрика элементов данной схемы
    template<typename T>
    void mk(T*& el){T* obj=new T();inner_elements.push_back(obj);el=obj;}
};

//! схема надо сделать чтобы была логическим элементом
class Tsxem_element
{
public:
    Tsxem_element()
    {
        sxem_glb->elements.insert(this);
    }
    static Tsxem* sxem_glb;
};

//! создаем таблицу виртуальных функций для элементов
class Telectro_element: public Tsxem_element
{
public:
    virtual void update()=0;
};

//! контекст менеджер для схем
class Tthis_sx
{
public:
    Tthis_sx(Tsxem* sx) {Tsxem_element::sxem_glb = sx;}
    ~Tthis_sx() {Tsxem_element::sxem_glb = nullptr;}
};

//! проводочек для соединения элементов
class Twire: public Tsxem_element
{
public:
    Twire():state(0),sxem(sxem_glb){}
    float state;
    Tsxem* sxem;
    set<Telectro_element*> users;
    operator float(){return state;}
    Twire& operator=(float v)
    {
        state=v;
        return (*this);
    }
};

//! класс источник напряжения
class Tusource: public Telectro_element
{
public:
    void update()
    {

    }
};

//! класс источник напряжения
class Tresiztor: public Telectro_element
{
public:
    void update()
    {
        
    }
};

//! класс однофазного трансформатора
class Tinductor
{
public:
    float R; //!сопротивления обмоток
    float S;//! Площадь седечника
    float mu;//! магнитная проницаемость
    void update()
    {
    }
};
