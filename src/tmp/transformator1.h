#pragma once
#include <sapr_element.h>

const int NO=2;
class Ttrans2_state
{
public:
    float I[NO];
};

//! класс однофазного трансформатора
class Ttrans2
{
    float A[NO][NO],B[NO][NO];
public:
    float w[NO]; //!число витков в обмотках
    float R[NO]; //!сопротивления обмоток
    float S;//! Площадь седечника
    float mu;//! магнитная проницаемость
    float M;//!  коэффициент взаимной индукции
    void update_matrix();
};

