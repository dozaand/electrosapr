#pragma once
#include <vector>
#include <set>
#include <matr_struct.h>
#include <upd_data.h>
//#include <dvp.h>
//#include <node.h>
class Tnode;
class Tdvp;
class Tsxem_solver;

class Sxem
{
    void maketree();
    int size_=0;
public:
    Tnode* ground_node;
    Sxem(): ground_node(nullptr) { Gs = this; }           //Gs-Global sxema
    ~Sxem() { Gs = nullptr; }
    static Sxem* Gs;
    std::set<Tnode*> nodes;
    std::set<Tdvp*> elements;
    int numberI; //число независимых токов в схеме
    Tnode* first_node() { return *nodes.begin(); }
    int numNodes() { return nodes.size(); };
    //! частичное исключение переменных методом узловых потенциалов возвращает размер модели
    int update_varlist(Tnode* ground_node=nullptr);
    void cont();
    std::vector<std::vector<float> > matrix;
//    void creatematrix(Tsxem_solver* slv);
    //! сохранение данных о топологии схемы
    void save_sxem(const char* file_name) const;
    //! загрузка данных о топологии схемы
    void load_sxem(const char* file_name) const;
    //! сохранение данных о текущем состоянии переменных состояния и параметрах схемы
    void save_sxem_state(const char* file_name) const;
    void load_sxem_state(const char* file_name) const;
    int size() const {return size_;}// размер алгебраической системы которую прийдется решать
    void pt() const;
    void i_check() const;
    void create_matrix(Tmatr_struct& mtr);
    void update_matrix(Tupd_data<double>& d);
    void update_q(Tupd_data<double>& d);
};

/*
class Tsxem_solver
{
    float alpha;
    float dt;
    bool need_update;// если надо пересчитать матрицу якоби системы
public:
    Sxem * sx;
    matr_umf<float> aB_p_A;//!< alpha*B + A для метода трапеций решаем уравнение (2*B/dt + A) x_{k+1} = (2*B/dt - A) x_k - 2 q
    matr_umf_lu<float> LU_aB_p_A;
    Tsxem_solver(Sxem* sx_):
        sx(sx_),
        B_p_aA(sx->size()),
        LU_B_p_aA(sx->size()) {}
    void set_dt(float dt_) {dt = dt_; alpha = 2. / dt;}
    // раешаем систему уравнений B x' + A x + q == 0
    // если много алгебраических уравнений то будем решать (2*B/dt + A) x_{k+1} = (2*B/dt - A) x_k - q
    void do_time_step();
    vector<float> x;//!< все неизвестные но идут в порядке u(state),u,I(state),I,omega,fi
    vector<float> q;//!< правая часть системы уравнений
    void update_varlist(Tnode* ground_node_);
};
*/
