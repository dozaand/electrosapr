#include <sxem.h>
#include <elements.h>

extern "C"
{
    void* create_sxem()
    {
        return new Sxem();
    }
    void delete_sxem(void* p)
    {
        delete (Sxem*)p;
    }
}