#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
import codecs
#import json
import yaml
#import argparse
#from mc2py.svld import sv

def to_float(x):
    try:
        y=float(x)
    except:
        y=x
    return y

def load_data(file_name="EnDb.xml"):

    with codecs.open(file_name,"r",encoding="cp1251") as f:
        data=f.read()

    nsmap={"xs":"http://www.w3.org/2001/XMLSchema","msdata":"urn:schemas-microsoft-com:xml-msdata","msprop":"urn:schemas-microsoft-com:xml-msprop"}

    root=etree.fromstring(data)
    tabdsc=root.xpath("/EnDb/xs:schema/xs:element/xs:complexType/xs:choice/xs:element",namespaces=nsmap)
    table_names = dict([dict(i.attrib).values() for i in tabdsc])
    table_fields={}
    for i in tabdsc:
        table_fields[i.attrib["name"]]=[[j.attrib["name"],j.attrib["{urn:schemas-microsoft-com:xml-msdata}Caption"]] for j in i.xpath("xs:complexType/xs:sequence/xs:element",namespaces=nsmap)]
    objs=root.xpath("/*/*",namespaces=nsmap)[1:]
    res=dict([(table_names[i],[dict(table_fields[i])]) for i in table_names])
    for obj in objs:
        res[table_names[obj.tag]].append(dict([[i.tag,to_float(i.text)] for i in obj]))

    res1={}
    for k,v in res.iteritems():
        x={}
        res1[k]=x
        for i in v:
            mark=i.pop("Mark")
            x[mark]=i

    return res1

def field_to_string(f):
    if type(f) is float:
        return u"{0:8.8g}".format(f)
    elif f is None:
        return '   none '
    else:
        return u"{0:20s}".format(f)

def sorted_save(res1,file_name="EnDb.yaml"):
    u"""сохраняю yaml но сортированый и табулированый"""
    mo=u"Марка оборудования"
    with codecs.open("EnDb.yaml","w",encoding="utf-8") as f:
        obj_types=sorted(res1.keys())
        for k in obj_types:
            f.write(k+":\n")
            table=res1[k]
            row_names=sorted(table.keys())
            row_names.remove(mo)
            col_names=sorted(table[mo].keys())
            row=table[mo]
    #        tabline=",".join([u" {0} : '{1}'".format(i,field_to_string(row[i])) for i in col_names])
            tabline=",".join([u' {0} : "{1}"'.format(i,row[i]) for i in col_names])
            k_tabline = u"   {0} : {{{1}}}".format(mo,tabline)+"\n"
            f.write(k_tabline)
            for key in row_names:
                row=table[key]
                tabline=",".join([u" {0} : {1}".format(i,field_to_string(row.get(i))) for i in col_names])
                k_tabline = u"   {0:23s} : {{{1}}}".format(key,tabline)+"\n"
                f.write(k_tabline)

res1=load_data()
sorted_save(res1)

#with codecs.open("EnDb.yaml","r",encoding="utf-8") as f:
#    xxx=yaml.load(f)

