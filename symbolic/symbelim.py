#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
пробую посмотреть что получится с символическим преобразованием схемы
"""

from sympy import Symbol, solve,symbols
import numpy as np
import sympy

class Tnode(object):
    def __init__(self,nm=None):
        self.name=nm
        self.elm=[]
    def __add__(self,obj):
        self.elm.append(obj)
        if hasattr(obj,"beg"):
            if hasattr(obj,"end"):
                raise "invalid connection"
            else:
                obj.end=self
        else:
            obj.beg=self
        return self


class Tdvp(object):
    def __init__(self,nm=None):
        self.name=nm

ground=Tnode("g")
up=Tnode("u")
e1=Tdvp()
e2=Tdvp()


i1,i2,i3,fi0,fi1,fi2,E,L,R=symbols('i1,i2,i3,fi0,fi1,fi2,E,L,R')
# пробую сделать rl цепочку





