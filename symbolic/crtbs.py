import mc2py.svld as svld

elmset={}
def crtrv(i,j):
    elmset["RV_{0}_{1}".format(i,j)]={"t" : "R","legs" : ["N_{0}_{1}".format(i,j),"N_{0}_{1}".format(i,j+1) ],"R" : 1}
def crtrg(i,j):
    elmset["RH_{0}_{1}".format(i,j)]={"t" : "R","legs" : ["N_{0}_{1}".format(i,j),"N_{0}_{1}".format(i+1,j) ],"R" : 1}


nn=20
for i in range(nn):
    for j in range(nn-1):
        crtrv(i,j)

for i in range(nn-1):
    for j in range(nn):
        crtrg(i,j)

elmset["RR"]={"t" : "R","legs" : ["G","N_0_0"],"R" : 1}
elmset["U"]={"t" : "T_bat","legs" : ["G","N_{0}_{0}".format(nn-1)], "u" : 1}

svld.sv(elmset,"bigsxem.yaml")
