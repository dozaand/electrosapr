#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""
пробую посмотреть что получится с символическим преобразованием схемы
цикл чтение - генерация
"""
from sympy.solvers import solve
from sympy import Symbol, symbols
#import sympy.solvers.solveset.linsolve as linsolve
import numpy as np
import sympy
import yaml
import codecs
import operator
import time
import networkx as nx
import pylab as plt
import mc2py.svld as svld


class T_element(object):
    def __init__(self,nm,dat):
        self.__dict__.update(dat)
        self.name=nm
    def dvp(self):
        return [self]

class T_dvp(T_element):
    def __init__(self,nm,dat):
        T_element.__init__(self,nm,dat)
        self.I=[]
    def is_L(self):
        return 0;
    def is_C(self):
        return 0;

class T_R(T_dvp):
    def __init__(self,nm,dat):
        T_dvp.__init__(self,nm,dat)

    def mtr(self,res,q):
        idx=self.index
        for i,d in self.I:
            res.append((idx,i,d*self.R))
        res.append((idx,self.ilegs[0],-1))
        res.append((idx,self.ilegs[1],1))
        q[idx]=0

class T_bat(T_dvp):
    def __init__(self,nm,dat):
        T_dvp.__init__(self,nm,dat)

    def mtr(self,res,q):
        idx=self.index
        res.append((idx,self.ilegs[0],-1))
        res.append((idx,self.ilegs[1],1))
        q[idx]=self.u

class T_sin(T_dvp):
    def __init__(self,nm,dat):
        T_dvp.__init__(self,nm,dat)

class T_L(T_dvp):
    def __init__(self,nm,dat):
        T_dvp.__init__(self,nm,dat)
    def is_L(self):
        return 1;

class T_C(T_dvp):
    def __init__(self,nm,dat):
        T_dvp.__init__(self,nm,dat)
    def is_C(self):
        return 1;

class T_tr(T_element):
    def __init__(self,nm,dat):
        T_element.__init__(self,nm,dat)
        self.L1=T_L(nm+"_L1",{"t":"T_L","L":dat["L"],"legs":dat["legs"][0]})
        ktr=dat["w2"]/dat["w1"]
        self.L2=T_L(nm+"_L2",{"t":"T_L","L":dat["L"]*ktr*ktr,"legs":dat["legs"][1]})

    def dvp(self):
        return [self.L1,self.L2]

# загрузка данных
with codecs.open("rl.yaml","r",encoding="utf-8") as f:
    data=yaml.load(f)

# генерируем элементы
elements={nm:globals()[dat["t"]](nm,dat) for nm,dat in data.iteritems()}
# составляем список двухполюсников
dvp=reduce(operator.add,[i.dvp() for i in elements.values()])
for i,v in enumerate(dvp):
    v.index=i
all_bus=list(set(reduce(operator.add,[i.legs for i in dvp])))
all_bus.remove("G")

# Исклчюаем лишние токи точнее заполняем списки токов
G=nx.Graph()
for elm in dvp:
    elm.legs[0]
    G.add_edge(elm.legs[0],elm)
    G.add_edge(elm,elm.legs[1])
cycles = nx.cycle_basis(G,"G")
n_I = len(cycles)

for iloop,cyc in enumerate(cycles):
    for i in range(0,len(cyc),2):
        elm=cyc[i]
        node=cyc[i+1]
        if elm.legs[1]==node:
            elm.I.append((iloop,1))
        else:
            elm.I.append((iloop,-1))

#делим узлы и ветви на переменные состояния и переменные котоые будем искть из алгебраичесих связей
Istate=set()
Ustate=set()
for elm in dvp:
    if elm.is_L():
        for i in elm.I:
            if i[0] not in Istate:
                Istate.add(i[0])
                break
    if elm.is_C():
        for i in elm.I:
            if i[0] not in Istate:
                Istate.add(i[0])
                break



# нумеруем узды и ветви
bus2index = {v:k+n_I for k,v in enumerate(all_bus)}
bus2index["G"]=-1
for el in dvp:
    el.ilegs=[bus2index[el.legs[0]],bus2index[el.legs[1]]]

self=dvp[3]
Ib,u1,u0,Ib_n,u1_n,u0_n=symbols("Ib,u1,u0,Ib_n,u1_n,u0_n")
f=Ib_n*self.R+u0_n-u1_n

#nx.draw_networkx(G)
#plt.show()



