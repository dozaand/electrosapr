#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
#import argparse
import os
import glob
import re
import cPickle
import codecs
import subprocess as sp
#from mc2py.which import Name as find_exe

incscapeapp=r"C:\apps\Inkscape\inkscape.com"
diashape_dir="C:/apps/Dia/shapes/*/*.shape"
nsmap={"dia":"http://www.daa.com.au/~james/dia-shape-ns","svg":"http://www.w3.org/2000/svg"}

#with open("shape.pkl","rb") as f:
#    db=cPickle.load(f)

def calc_bbox(db):
    with codecs.open("tmp.svg","w",encoding="utf-8") as f:
        f.write('<svg><g style="fill:none;stroke:#000000;stroke-width:0.2">\n')
        i=0
        for k,v in db.iteritems():
            if "xlink:href" not in v['img_no_ns']:
                lin=u"<g id = 'xxid{id}'>{img}</g>\n\n".format(id=i,img=v['img_no_ns'])
                f.write(lin)
                i+=1
        f.write("</g></svg>")

    data=sp.check_output(incscapeapp+" -S tmp.svg",shell=1)
    figdata=[map(float,i.split(",")[1:]) for i in data.split() if i.startswith("xxid")]
    i=0
    for k,v in db.iteritems():
        if "xlink:href" not in v['img_no_ns']:
            v["bbox"]=figdata[i]
            i+=1

#i=ur"C:\PROJECTS\.dia\shapes\electric3\vinductor.shape"
def create_shape_db():
    dia_shape_file=os.path.join(os.environ["HOME"],".dia","shapes","*","*.shape")
    files=glob.glob(dia_shape_file)+glob.glob(diashape_dir)
    db={}
    for filename in files:
        try:
            tree = etree.parse(filename)
            root = tree.getroot()

            res={}
            res["props"]=[[i.attrib["name"],i.attrib["type"]] for i in root.xpath("./dia:ext_attributes/dia:ext_attribute",namespaces=nsmap)],
            res["name"]=root.xpath("./dia:name",namespaces=nsmap)[0].text
            try:
                res["dsc"]=root.xpath("./dia:description",namespaces=nsmap)[0].text
            except:
                pass
            try:
                res["dwidth"] = root.xpath("./dia:default-width",namespaces=nsmap)[0].text
                res["dheight"] = root.xpath("./dia:default-height",namespaces=nsmap)[0].text
            except:
                pass
            res["connections"]=[[float(i.attrib["x"]),float(i.attrib["y"])] for i in root.xpath("./dia:connections/dia:point",namespaces=nsmap)]
            try:
                res["height"] = float(root.xpath("./svg:svg",namespaces=nsmap)[0].attrib['height'])
                res["width"] = float(root.xpath("./svg:svg",namespaces=nsmap)[0].attrib['width'])
            except:
                res["height"]=5
                res["width"]=5
            img=etree.tostring(root.xpath("./svg:svg",namespaces=nsmap)[0], pretty_print=True)
            res["img_no_ns"] = re.sub("</?svg[^>]*>","",re.sub(" +"," ",re.sub("svg:","",img)))
            #res["shortname"]=res["name"].split(" - ")[-1]
            db[res["name"]]=res
        except:
            print("fail on file",filename)
    calc_bbox(db)
    with open("shape.pkl","wb") as f:
        cPickle.dump(db,f,2)



if __name__ == '__main__':
    create_shape_db()
    """    parser = argparse.ArgumentParser(
        prog="fdl2h5", description="convert dia to topoljy data in json format")
    parser.add_argument("input", help=u"input file")
    parser.add_argument("--output", "-o", nargs="?", help=u"output file name")
    args=parser.parse_args()
    dia_to_json(args.input,args.output)"""
