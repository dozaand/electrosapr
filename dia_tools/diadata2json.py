#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
from lxml import etree
import json
import argparse
#from zipfile import ZipFile
import subprocess as sp
import cPickle
import re

def get_root(input_file):
    try:
        data=sp.check_output("7z x {0} -so".format(input_file))
        root=etree.fromstring(data)
    except:
        tree = etree.parse(input_file)
        root = tree.getroot()
    return root

def to_points(txt):
    return map(float,txt.split(","))

def dia_to_data(root):
    nsmap={"dia":"http://www.lysator.liu.se/~alla/dia/"}
    objs=root.xpath("//*/dia:object",namespaces=nsmap)
    res=[]
    i=objs[3]
    for i in objs:
        v={}
        v["id"]={"type":i.attrib["type"],"name":i.attrib["id"]}
        try:
            v["pos"]=map(float,i.xpath("./dia:attribute[@name='obj_pos']/dia:point/@val",namespaces=nsmap)[0].split(','))
        except:
            pass
        try:
            v["bounding_box"]=map(float,re.split("[,;]",i.xpath("./dia:attribute[@name='obj_bb']/dia:rectangle/@val",namespaces=nsmap)[0]))
        except:
            pass
        try:
            v["line_color"]=i.xpath("./dia:attribute[@name='line_colour']/dia:color/@val",namespaces=nsmap)[0]
        except:
            pass
        try:
            v["line_width"]=float(i.xpath("./dia:attribute[@name='line_width']/dia:real/@val",namespaces=nsmap)[0])
        except:
            pass
        try:
            v["orth_points"] = map(to_points,i.xpath("./dia:attribute[@name='orth_points']/dia:point/@val",namespaces=nsmap))
            v["orth_orient"] = map(int,i.xpath("./dia:attribute[@name='orth_orient']/dia:point/@val",namespaces=nsmap))
        except:
            pass
        try:
            v["conn_endpoints"] = map(to_points,i.xpath("./dia:attribute[@name='conn_endpoints']/dia:point/@val",namespaces=nsmap))
        except:
            pass

        custom=[]
        for node in i:
            if "name" in node.attrib:
                nm_full=node.attrib["name"]
                if nm_full.startswith("custom:"):
                    nm=nm_full.split(":")[1]
                    if "val" in node[0].attrib:
                        val=node[0].attrib["val"]
                    else:
                        try:
                            val=node[0].text[1:-1]
                        except:
                            print "qq"
                    custom.append([nm,val])
        if custom:
            v[u"p"]=custom
        conn=i.xpath("./dia:connections/dia:connection",namespaces=nsmap)
        if conn:
            v["conn"]=[]
            for j in conn:
                v["conn"].append(dict(j.attrib))
        res.append(v)

    return res


def save_as_html(root,outfile):
    lines=[]
    topx=10 # мастабирование до пикселов
    objdata=dia_to_data(root)
    for shem_elt in objdata:
        try:
            shape=shapelib[shem_elt["id"]["type"]]
            x0,y0,w0,h0=shape["bbox"]
            x1,y1,x11,y11=shem_elt['bounding_box']
            w1=x11-x1;h1=y11-y1
            scale_x=w1/w0*topx
            scale_y=h1/h0*topx
            shift_x=(x1*topx-x0*scale_x)
            shift_y=(y1*topx-y0*scale_y)
            obj_line=u'<g transform="matrix({scale_x},0,0,{scale_y},{shift_x},{shift_y})">{body}</g>'.format(scale_x=scale_x,scale_y=scale_y,shift_x=shift_x,shift_y=shift_y,body=shape['img_no_ns'])
            lines.append(obj_line)
        except:
            if shem_elt["id"]["type"]=='Standard - ZigZagLine':
                pointset=" ".join(map(lambda x:",".join(map(str,x)),shem_elt["orth_points"]))
                obj_line='<g transform="scale({topx},{topx})"><polyline points="{pnt}"/></g>'.format(pnt=pointset,topx=topx)
                lines.append(obj_line)
            elif shem_elt["id"]["type"]=='Standard - Line':
                pnt=shem_elt["conn_endpoints"]
                obj_line='<g transform="scale(10,10)"><line x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}"/></g>'.format(x1=pnt[0][0],y1=pnt[0][1],x2=pnt[1][0],y2=pnt[1][1])
                lines.append(obj_line)
            else:
                print shem_elt["id"]["type"]

    html_body=ur"""
    <html>
    <body>
    <h1>My first SVG</h1>
    <svg width="600" height="600" fill="none" stroke-width="0.2" stroke="black">
    {body}
    </svg>
    </body>
    </html>""".format(body="\n".join(lines))
    with codecs.open(outfile,"w",encoding="utf-8") as f:
        f.write(html_body)



def dia_to_json(root,input_file,output_file):

    if not output_file:
        output_file=os.path.splitext(input_file)[0]+".json"

    res = dia_to_data(root)

    with codecs.open(output_file,"w",encoding="utf-8") as f:
        json.dump(res,f, indent=2,encoding="utf-8",ensure_ascii=False)

#dia_to_json("simplesxem2.dia","x.json")
# загрузка библиотеки элементарных форм
with open("shape.pkl","rb") as f:
    shapelib=cPickle.load(f)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="fdl2h5", description="convert dia to topoljy data in json format")
    parser.add_argument("input", help=u"input file")
    parser.add_argument("--output", "-o", nargs="?", help=u"output file name")
    args=parser.parse_args()
    root=get_root(args.input)
    save_as_html(root,args.output)
#    dia_to_json(root,args.input,args.output)
